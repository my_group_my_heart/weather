import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(BaseLayout());
}

class BaseLayout extends StatelessWidget {
  double rowPadding = 24.0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/sky.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(
                    "Bangkok",
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 90,
                ),
                const Text(
                  " 36°",
                  style: TextStyle(
                      fontSize: 100,
                      fontWeight: FontWeight.w900,
                      color: Colors.white),
                ),
                const Text(
                  "Cloudy",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      color: Colors.white),
                ),
                const Text(
                  "Real 43°",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w900,
                      color: Colors.white),
                ),
                const SizedBox(
                  height: 50.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Text(
                          "12:00",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        ),

                        Padding(
                          padding: EdgeInsets.all(rowPadding),
                          child: Column(
                            children: [
                              const Padding(
                                padding: EdgeInsets.symmetric(vertical: 0.0),
                                child: Icon(
                                  Icons.wb_sunny_outlined, size: 70,
                                  color: Colors.white,
                                  // color: Colors.indigo.shade800,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 1.0,
                        ),
                        Text(
                          "36°",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                        const SizedBox(
                          height: 10.0,
                        ),
                      ],

                    )


                  ],
                ),

              ],
            )), /* add child content here */
      ),
    );
  }
}